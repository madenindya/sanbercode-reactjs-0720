// soal 1

var loop1 = ""
var loop2 = ""

var i = 1;
while (i <= 10) {
    loop1 += (i*2) + " - I love coding\n";
    loop2 = (i*2) + " - I will become a frontend developer\n" + loop2;
    i++;
}

result = "LOOPING PERTAMA\n" + loop1  + "LOOPING KEDUA\n" + loop2;
console.log(result);


// soal 2

for (i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 == 1) {
        console.log(`${i} - I Love Coding`);
    } else if (i % 2 == 1) {
        console.log(`${i} - Santai`);
    } else {
        console.log(`${i} - Berkualitas`)
    }
}
console.log();


// soal 3

var pagar = "#";
for (i = 0; i < 7; i++) {
    console.log(pagar);
    pagar += "#";
}
console.log();


// soal 4

var kalimat="saya sangat senang belajar javascript";
var tokens = kalimat.split(" ");
console.log(tokens);
console.log();


// soal 5

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
for (var i of daftarBuah) {
    console.log(i)
}
console.log();
