// soal 1

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var result1 = `${kataPertama} ${kataKedua.charAt(0).toUpperCase()}${kataKedua.slice(1)} ${kataKetiga} ${kataKeempat.toUpperCase()}`;
console.log(result1);


// soal 2

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var result2 = (+kataPertama) + (+kataKedua) + (+kataKetiga) + (+kataKeempat);
console.log(result2);


// soal 3

var kalimat = 'wah javascript itu keren sekali';
var tokens = kalimat.split(" ");

var kataPertama = kalimat.substring(0, 3);
var kataKedua = tokens[1];
var kataKetiga = tokens[2];
var kataKeempat = tokens[3];
var kataKelima = tokens[4];

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);


// soal 4

var nilai = 75;
var idx = 'O';

if (nilai >= 80) {
    idx = "A";
} else if (nilai >= 70) {
    idx = "B";
} else if (nilai >= 60) {
    idx = "c";
} else if (nilai >= 50) {
    idx = "D";
} else {
    idx = "E";
}
console.log(idx);


// soal 5

var tanggal = 3;
var bulan = 2;
var tahun = 2020;

switch (bulan) {
    case 1:
        bulan = "Januari"
        break;
    case 2:
        bulan = "Februari"
        break;
    case 3:
        bulan = "Maret"
        break;
    case 4:
        bulan = "April"
        break;
    case 5:
        bulan = "Mei"
        break;
    case 6:
        bulan = "Juni"
        break;
    case 7:
        bulan = "Juli"
        break;
    case 8:
        bulan = "Agustus"
        break;
    case 9:
        bulan = "September"
        break;
    case 10:
        bulan = "Oktober"
        break;
    case 11:
        bulan = "November"
        break;
    case 12:
        bulan = "Desember"
        break;
    default:
        throw new Error("Invalid Month");
}
result5 = `${tanggal} ${bulan} ${tahun}`
console.log(result5)
