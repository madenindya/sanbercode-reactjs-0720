// soal 1

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];

var objDaftarPeserta = {
  nama: arrayDaftarPeserta[0],
  jenisKelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunLahir: arrayDaftarPeserta[3],
};
console.log(objDaftarPeserta);
console.log();


// soal 2

var fruits = [
  { nama: "strawberry", warna: ["merah"], "ada bijinya": false, harga: 9000 },
  { nama: "jeruk", warna: ["oranye"], "ada bijinya": true, harga: 8000 },
  { nama: "Semangka", warna: ["hijau", "merah"], "ada bijinya": true, harga: 10000 },
  { nama: "Pisang", warna: ["kuning"], "ada bijinya": false, harga: 5000 },
];

for (var key in fruits[0]) {
    var value = fruits[0][key]
    if (key == "ada bijinya") {
        value = value ? "ada" : "tidak";
    } else if (key == "warna") {
        var tmp = "";
        if (value.length > 0) {
            tmp = value[0];
        }
        for (var i = 1; i < value.length - 1; i++) {
            tmp += ", " + value[i];
        }
        if (value.length > 1) {
            tmp += " & " + value[value.length - 1]
        }
        value = tmp;
    }
    console.log(`${key}: ${value}`)
}
console.log();


// soal 3

var dataFilm = []

function addFilmObj(film) {
    var params = ["nama", "durasi", "genre", "tahun"];
    for (var param of params) {
        if (!film[param]) {
            throw Error("INVALID_OBJECT")
        }
    }
    dataFilm.push(film);
}
function addFilmDetails(nama, durasi, genre, tahun) {
    dataFilm.push({
        nama, durasi, genre, tahun
    });
}

addFilmObj({
    nama: "nama-1",
    durasi: "30 mnt",
    genre: "mystery",
    tahun: 1990
})
addFilmDetails("nama 2", "1 jam", "action", 2000);

console.log(dataFilm);
console.log();


// soal 4

class Animal {
    constructor(name) {
        this.nama = name;
        this.kaki = 4;
        this.darah_dingin = false;
    }

    get name() {
        return this.nama;
    }
    set name(x) {
        this.nama = x;
    }

    get legs() {
        return this.kaki;
    }
    set legs(x) {
        this.kaki = x;
    }

    get cold_blooded() {
        return this.darah_dingin;
    }
    set cold_blooded(x) {
        this.darah_dingin = x;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
    constructor(name) {
        super(name);
        this.kaki = 2
    }

    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
        this.cold_blooded = true;
    }

    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"

console.log();


// soal 5

class Clock {

    constructor (data) {
        this.template = data.template;
        this.timer = null;
        this.r = this.render
    }

    start() {
        this.render()
        this.timer = setInterval(this.render.bind(this), 1000);
    }

    stop() {
        if (this.timer) {
            clearInterval(this.timer);
        }
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);

        console.log(output);
      }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();
