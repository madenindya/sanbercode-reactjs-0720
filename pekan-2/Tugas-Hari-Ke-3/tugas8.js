// soal 1

const luasLingkaran = (r) => {
    const luas = Math.PI * r * r;
    return luas;
}
const kelilingLingkaran = (r) => {
    const keliling = Math.PI * 2 * r;
    return keliling;
}

const r = 7;
const luas = luasLingkaran(r);
const keliling = kelilingLingkaran(r)
console.log(luas, keliling);


// soal 2

const addKata = (kalimat, kata) => {
    kalimat += `${kata} `
    return kalimat;
}

const katas = ["saya", "adalah", "seorang", "frontend", "developer"];
let kalimat = ""
for (const kata of katas) {
    kalimat = addKata(kalimat, kata);
}
console.log(kalimat);



// soal 3

class Book {
    constructor(name, totalPage, price) {
      this.name = name;
      this.totalPage = totalPage;
      this.price = price;
    }

    print() {
        console.log(`Nama: ${this.name}`);
        console.log(`Jumlah Halaman: ${this.totalPage}`);
        console.log(`Harga: ${this.price}`);
    }
}

class Komik extends Book {
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price);
        this.isColorful = isColorful;
    }

    print() {
        super.print();
        console.log(`Berwarna: ${this.isColorful ? 'ya' : 'tidak'}`);
    }
}

const doraemon =  new Komik("Doraemon", 100, 6000, false);
doraemon.print();
