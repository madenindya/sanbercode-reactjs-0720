import React from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <div class="col-50 col-border">
        <h1>Form Pembelian Buah</h1>
        <div class="text-left">
          <form>
            <div class="col-row">
              <div class="col-40 text-bold">Nama Pelanggan</div>
              <div class="col-60">
                <input type="text" name="name" />
              </div>
            </div>
            <div class="col-row">
              <div class="col-40 text-bold item-checkbox">Daftar Item</div>
              <div class="col-60">
                <input type="checkbox" id="semangka" name="semangka" value="semangka" />
                <label for="semangka">Semangka</label>
                <br></br>
                <input type="checkbox" id="jeruk" name="jeruk" value="jeruk" />
                <label for="jeruk">Jeruk</label>
                <br></br>
                <input type="checkbox" id="nanas" name="nanas" value="nanas" />
                <label for="nanas">Nanas</label>
                <br></br>
                <input type="checkbox" id="salak" name="salak" value="salak" />
                <label for="salak">Salak</label>
                <br></br>
                <input type="checkbox" id="anggur" name="anggur" value="anggur" />
                <label for="anggur">Anggur</label>
                <br></br>
              </div>
            </div>
            <div class="col-row">
              <input type="submit" value="Kirim" />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default App;
