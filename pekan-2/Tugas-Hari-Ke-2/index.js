// di index.js
var readBooks = require("./callback.js");

var books = [
    { name: "LOTR", timeSpent: 3000 },
    { name: "Fidas", timeSpent: 2000 },
    { name: "Kalkulus", timeSpent: 4000 },
    { name: "komik", timeSpent: 1000 },
];

// soal 1

function read(i, timeleft) {
    readBooks(timeleft, books[i], function (timeleft) {
        i += 1;
        if (i < books.length) {
            read(i, timeleft);
        }
    });
}
read(0, 10000);
