var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// soal 2

function read(i, time) {
    readBooksPromise(time, books[i])
        .then(function (time) {
            i++;
            if (i < books.length) {
                read(i, time);
            }
        })
        .catch(function (time) {
            console.log("Timeout");
        });
}
read(0, 10000);
